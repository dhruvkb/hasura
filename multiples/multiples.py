def get_substitutions(m):
    """
    Return a dictionary of magic numbers and substitution strings

    :param m: the number of magic numbers to accept substitutions for
    """

    substitutions = {}
    for i in range(m):
        magic_number, substitution = input().split(" ")
        magic_number = int(magic_number)
        substitutions[magic_number] = substitution
    return substitutions


def replace_magic_numbers(n, array, substitutions):
    """
    Replace all multiples of the magic numbers with the substitution strings

    :param n: the number to loop the array to
    :param array: the array containing the numbers 0 to n, inclusive
    :param substitutions: the map of magic numbers and their substitute words
    """
    
    for magic_number in substitutions.keys():
        for i in range(magic_number, n + 1, magic_number):
            if array[i] == i:
                array[i] = substitutions[magic_number]
            else:
                array[i] = f"{array[i]}{substitutions[magic_number]}"


def print_array_by_lines(array):
    """
    Print the array one item per line

    :param array: the array to print
    """

    for item in array:
        print(item)


n = int(input())  # Number to loop to
array = list(range(n + 1))

m = int(input())  # Number of magic numbers
substitutions = get_substitutions(m)

replace_magic_numbers(n, array, substitutions)
print_array_by_lines(array[1:])
