# Multiples

This program replaces the given set of numbers and their multiples with a 
substitute word.

## Command

To run the program, invoke it from the terminal as follows:
```
$ python3 multiples.py < input.txt > output.txt
```

## Sample input

Make sure the file `input.txt` contains the following:
```
6
2
2 Idli
3 Sambar
```

## Expected output

To check the validity of the program, compare `output.txt` with the following:
```
1
Idli
Sambar
Idli
5
IdliSambar
```
