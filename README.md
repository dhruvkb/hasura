# Hasura recruitment test

## Questions

The wonderfully designed test, beautifully typeset (with LaTeX?), containing
challenging and interesting questions can be found in `yaksha.pdf`.

This test comprised of two questions.

1. Multiples
2. Flipbook

## Solutions

Both solutions are present in eponymous subdirectories.

## How-to ...?

Both subdirectories contain `README.md` files describing the solution. The code 
itself is pretty well-documented and readable. In addition to that, one example
input file, named `input.txt`, is provided for both problems with the expected
outcome mentioned in the aforementioned `README.md` file.

Thanks Hasura!