# FliPy

**FliPy**, pronounced _flippy_, is a language for defining flipbooks. It uses a
simple declarative syntax to describe the contents of a flipbook and the 
operations to perform upon them for animation.

## Concepts

A flipbook is an ordered collection of pages. Each page contains a collection
of sprites which are combinations of images and metadata. This metadata
describes the position, size and rotation of the image and vary from page to 
page as described by functions.

From this description of a flipbook, we can see that the classes involved are:

- book
- page
- sprite
- function
- metadata
- image

These classes are all defined in `models.py`.

## Primitives

The way the language works is, we first register images and functions into the
scope, assigning them names for reference. Then we place these images onto 
series of pages where the function determines the attributes of the image for
every page individually.

These primitives are all defined in `primitives.py`.

### `set_dimensions`

Set the dimensions of the flipbook to the given width and height. Syntax:
```
set_dimensions <width> <height>
```

### `add_pages`

Add the given number of pages to the flipbook. Syntax:
```
add_pages <page_count> 
```

### `register_image`

Create an image from a path. Syntax:
```
register_image <name> <path>
```

### `register_function`

Create a function from a string. Syntax:
```
register_function <name> <func_string>
```

The function definition has some rules to be followed. The function must be
defined using valid Python syntax. The `func_string` must be defined in a single
line. The name of the function in the string must be func and must accept a 
single argument which will be the page number.

The return of the function must be two tuples and a value. The tuples are, in
order, as follows:

- position (`x`, `y`)
- size (`w`, `h`)
- rotation in radians `r`

```
def func(pn): return (x, y), (w, h), r
```

You may use `math` functions, such as `math.sin` and `math.cos`, in the
expressions `x`, `y`, `w`, `h` and `r`.

So the if we defined `r` as `(2*math.pi*pn/100)`, the image would make a
complete rotation over 100 pages.

### `place_image`

Place the given image on the given pages using the given function. Syntax:
```
place_image <image> <function> <page_from> <page_to>
```

### `end`

Convert the flipbook into a JSON file. Syntax:
```
end
```

## Command

The language takes in `.txt` files and produces `.json` files. For simplicity,
no new file types have been defined.

To run the program, invoke it from the terminal as follows:
```
$ python3 flipbook.py < input.txt > output.json
```

## Sample input

An example flipbook input file can be found in `input.txt`.