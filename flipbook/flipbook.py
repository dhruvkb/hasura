# from flipbook.models import Book, Page, Sprite, Image
from primitives import functions as primitives

title = input()  # The very first line should be the title of the flipbook
flipbook = primitives['start'](title)

command = ""
while True:
    # Read a line from STDIN
    command = input()

    # Empty lines and those beginning with '#' are ignored
    if command == '' or command[0] == '#':
        continue

    # Process the command
    function, *args = command.split(' ')
    primitives[function](flipbook, *args)

    # The very last line should be 'end'
    if function == 'end':
        break
