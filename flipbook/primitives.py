from fpdf import FPDF

from models import Book, Page, Sprite, Image, Function


def start(title):
    """
    Start a flipbook with the given name.
    """

    return Book(title=title)


def set_dimensions(flipbook, width, height):
    """
    Set the dimensions of the flipbook to the given width and height.

    Syntax:
        set_dimensions <width> <height>
    """

    width = int(width)
    height = int(height)
    flipbook.dimensions = (width, height)


def add_pages(flipbook, page_count):
    """
    Add the given number of pages to the flipbook.

    Syntax:
        add_pages <page_count>
    """

    page_count = int(page_count)
    current_count = len(flipbook.pages)
    flipbook.pages += [Page(current_count + i) for i in range(page_count)]


def register_image(flipbook, name, path):
    """
    Create an image from a path.

    Syntax:
        register_image <name> <path>
    """

    flipbook.images[name] = Image(name=name, path=path)


def register_function(flipbook, name, *func_string):
    """
    Create a function from a string.
    
    Syntax:
        register_function <name> <func_string>
    """

    func_string = ' '.join(func_string)
    flipbook.functions[name] = Function(func_string=func_string)


def place_image(flipbook, image, function, page_from, page_to):
    """
    Place the given image on the given pages using the given function.

    Syntax:
        place_image <image> <function> <page_from> <page_to>
    """

    image = flipbook.images[image]
    function = flipbook.functions[function]

    for page_number in range(int(page_from), int(page_to) + 1):
        page = flipbook.pages[page_number]
        metadata = function(page_number)
        sprite = Sprite(image=image, metadata=metadata)
        page.sprites.append(sprite)


def end(flipbook):
    """
    Convert the flipbook into a PDF file and output JSON data.
    """

    flipbook.print()

    pdf = FPDF(format=flipbook.dimensions)
    for page in flipbook.pages:
        pdf.add_page()
        for sprite in page.sprites:
            image = sprite.image
            metadata = sprite.metadata

            x = metadata.position[0] - metadata.size[0] / 2
            y = metadata.position[1] - metadata.size[1] / 2

            # TODO: Add support for printing rotations
            pdf.image(
                image.path,
                x,
                y,
                metadata.size[0],
                metadata.size[1]
            )
    pdf.output(f'{flipbook.title}.pdf', 'F')


functions = {
    'start': start,

    'set_dimensions': set_dimensions,

    'add_pages': add_pages,

    'register_image': register_image,
    'register_function': register_function,

    'place_image': place_image,

    'end': end,
}
