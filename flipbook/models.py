import json
import math


class Book:
    """
    A book is a collection of pages, each of which is a collection of sprites.
    """

    def __init__(self, title,
                 dimensions=None, images=None, functions=None, pages=None):
        """
        Create a new book with the given title.

        :param title: the title of the flipbook
        """

        self.title = title
        self.dimensions = dimensions if dimensions else (100, 100)

        self.pages = pages if pages else []

        self.images = images if images else {}
        self.functions = functions if functions else {}

    def __dict__(self):
        """
        Return a dictionary representation of the flipbook, recursively
        making dictionaries of child objects.
        """

        dictionary = {
            'title': self.title,
            'dimensions': {
                'width': self.dimensions[0],
                'height': self.dimensions[1],
            },
            'page_count': len(self.pages),
            'pages': [page.__dict__() for page in self.pages],
        }
        return dictionary

    def print(self):
        print(json.dumps(self.__dict__()))


class Page:
    """
    Pages make up the content of a book. A page provides a 2D canvas on which
    to place sprites.
    """

    def __init__(self, page_number, sprites=None):
        """
        Create a new page with the given page number.

        :param page_number: the page number of this page
        """

        self.page_number = page_number
        self.sprites = sprites if sprites else []

    def __dict__(self):
        """
        Return a dictionary representation of the flipbook, recursively
        making dictionaries of child objects.
        """

        dictionary = {
            'page_number': self.page_number,
            'sprites': [sprite.__dict__() for sprite in self.sprites],
        }
        return dictionary


class Sprite:
    """
    A sprite is a relationship between an image and some metadata.
    """

    def __init__(self, image, metadata=None):
        """
        Create a new sprite with the given image.

        :param image: the image that the sprite will display
        """

        self.image = image
        self.metadata = metadata if metadata else Metadata()

    def __dict__(self):
        """
        Return a dictionary representation of the flipbook, recursively
        making dictionaries of child objects.
        """

        dictionary = {
            'image': self.image.__dict__(),
            'metadata': self.metadata.__dict__(),
        }
        return dictionary


class Function:
    """
    A function returns a sprite as a function of page number.
    """

    def __init__(self, func_string):
        """
        Create a new function with the given function string.

        :param func_string: a string defining the functions logic
        """

        self.func_string = func_string

    def __call__(self, page_number):
        """
        Execute the function described by the function string by passing the
        page number to it as an argument.
        """

        scope = {
            'math': math,
        }
        exec(self.func_string, scope)
        return Metadata(*scope['func'](page_number))


class Metadata:
    """
    Metadata describes an image on a page with attributes such as size, position
    and orientation.
    """

    def __init__(self, position=(0, 0), size=(0, 0), rotation=0):
        """
        Create a new metadata instance.
        """

        self.position = position
        self.size = size
        self.rotation = rotation

    def __dict__(self):
        """
        Return a dictionary representation of the flipbook, recursively
        making dictionaries of child objects.
        """

        dictionary = {
            'position': {
                'x': self.position[0],
                'y': self.position[1],
            },
            'size': {
                'width': self.size[0],
                'height': self.size[1],
            },
            'rotation': self.rotation,
        }
        return dictionary


class Image:
    """
    An image is any 2D arrangement of pixels with color information.
    """

    def __init__(self, name, path):
        """
        Create a new image with the given name and path.

        :param name: the name to use when referring to the image
        :param path: the path to the image asset on the disk
        """

        self.name = name
        self.path = path

    def __dict__(self):
        """
        Return a dictionary representation of the flipbook, recursively
        making dictionaries of child objects.
        """

        dictionary = {
            'name': self.name,
            'path': self.path,
        }
        return dictionary
